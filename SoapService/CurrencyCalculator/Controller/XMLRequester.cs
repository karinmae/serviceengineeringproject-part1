﻿using CurrencyCalculator.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace CurrencyCalculator.Controller
{
    public class XMLRequester
    {
        static string _URL = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
        private List<string> validCodes = new List<string>();
        private List<Currency> currencies = new List<Currency>();

        public XMLRequester()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(_URL);
            XmlElement root = xml.DocumentElement;
            XmlNode cube = root.LastChild.LastChild;
            XmlNodeList cubes = cube.ChildNodes;

            this.currencies = convertXMLToCurrencies(this.currencies, cubes);

        }

        private List<Currency> convertXMLToCurrencies(List<Currency> currencyList, XmlNodeList cubes)
        {
            foreach (XmlNode item in cubes)
            {
                string code = item.Attributes["currency"].Value;
                float rate = float.Parse(item.Attributes["rate"].Value, System.Globalization.CultureInfo.InvariantCulture);

                Currency cur = new Currency(code, rate);
                addToValidCodes(code);
                currencyList.Add(cur);
            }
            //eur must be added too
            Currency cur1 = new Currency("EUR", 0);
            addToValidCodes("EUR");
            currencyList.Add(cur1);

            return currencyList;
        }

        public float convertToCurrencyFromEuro(float euro_price, string code)
        {
            float convertedPrice = 0;

            if (code == "EUR")
            {
                return euro_price;
            }
            else if (validCodes.Contains(code))
            {
                foreach (Currency cur in getCurrencies())
                {
                    if (cur.code == code)
                    {
                        convertedPrice = cur.calculateCurrencyRate(euro_price, cur);
                        break;
                    }
                }
            }

            return convertedPrice;
        }

        public float getCrossRates(String from_code, String to_code, float price)
        {
            if (from_code == to_code)
            {
                return price;
            }

            Currency from = new Currency();
            Currency to = new Currency();

            if (validCodes.Contains(from_code) && validCodes.Contains(to_code))
            {
                foreach (Currency cur in getCurrencies())
                {

                    if (cur.code == from_code)
                    {
                        from = cur;
                    }
                    if (cur.code == to_code)
                    {
                        to = cur;
                    }
                }

                return from.calculateCrossCurrency(price, to, from);
            }
            else
            {
                return -1;  //code not in list
            }

        }

        public void addToValidCodes(String code)
        {
            validCodes.Add(code);
        }

        public List<Currency> getCurrencies()
        {
            return currencies;
        }

        public void setCurrencies(List<Currency> currencies)
        {
            this.currencies = currencies;
        }
    }
}