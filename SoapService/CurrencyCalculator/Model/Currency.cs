﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CurrencyCalculator.Model
{
    public class Currency
    {
        public String code;
        public float rate;

        public Currency()
        {

        }

        public Currency(String code, float rate)
        {
            this.code = code;
            this.rate = rate;
        }

        public float calculateCurrencyRate(float euro_price, Currency currency)
        {
            return euro_price * currency.rate;
        }

        public float calculateCrossCurrency(float price, Currency to, Currency from)
        {
            float euro_price = 0;

            if (from.code == "EUR")
            {
                euro_price = price;
            }
            else if (to.code == "EUR")
            {
                return convertToEuroPrice(from, price);
            }
            else
            {
                euro_price = convertToEuroPrice(from, price);
            }

            return calculateCurrencyRate(euro_price, to);
        }

        public float convertToEuroPrice(Currency cur, float price)
        {
            if (cur.rate > 0)
            {
                return price / cur.rate;
            }
            return 0;
        }
    }
}