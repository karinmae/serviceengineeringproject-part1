﻿using CurrencyCalculator.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace CurrencyCalculator
{
    /// <summary>
    /// Summary description for CurrencyCalculatorService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CurrencyCalculatorService : System.Web.Services.WebService
    {

        [WebMethod]
        public float EuroToCurrencyConverter(String code, float euro_price)
        {
            XMLRequester requester = new XMLRequester();
            return requester.convertToCurrencyFromEuro(euro_price, code);

        }

        [WebMethod]
        public float GetCrossRate(String from_code, String to_code, float price)
        {
            XMLRequester requester = new XMLRequester();
            return requester.getCrossRates(from_code, to_code, price);
        }
    }
}
