﻿using ServiceConsumer.CurrencyCalculatorServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceConsumer
{
    class Program
    {
        static void Main(string[] args)
        {
            CurrencyCalculatorServiceSoapClient client = new CurrencyCalculatorServiceSoapClient();
            Console.WriteLine("client initiated");
            Console.WriteLine("10 Euro to USD = ");

            float usd = client.EuroToCurrencyConverter("USD", 10);
            Console.WriteLine(usd);

            Console.WriteLine(usd + " USD to JPY = ");

            float jpy = client.GetCrossRate("USD", "JPY", usd);
            Console.WriteLine(jpy);
            Console.ReadLine();   
        }
    }
}
