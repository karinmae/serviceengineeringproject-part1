## Description
The car-rental service provides REST interfaces to interact with the car rental shop.
There are 3 different kinds of authorization:

## Base url
base url: http://ec2-54-93-34-240.eu-central-1.compute.amazonaws.com:8080/car-rental-rest-service/

### Login
To login to the service and retrieve a Authorization cookie users have to send a POST request to /login

### Authorized Admin
Requests can only be done with a logged in admin account, by sending the Authorization cookie with the request.

### Authorized User
Requests can only be done with a logged in user account, by sending the Authorization cookie with the request.

### Unauthorized
Every user can send the request. There is no Authorization cookie needed



## /location/{id}
GET	Retrieve location from given id. If id is empty returns all locations.
	Returns json array with location objects.
	
	ALLOWED_ROLE: ALL
	
POST Upload location .
	Content: valid location json object.
	
	ALLOWED_ROLE: ADMIN
	
DELETE Remove location from given id.

	ALLOWED_ROLE: ADMIN
	
PUT	Update location from given id.
	Content: valid location json object.
	
	ALLOWED_ROLE: ADMIN

## /car/{id}
GET	Retrieve car from given id. If id is empty returns all cars.
	Returns json array with location objects.
	
	ALLOWED_ROLE: ALL
	
POST Upload location 
	Content: valid car json object.
	
	ALLOWED_ROLE: ADMIN
	
DELETE Remove car from given id.

	ALLOWED_ROLE: ADMIN
	
PUT	Update car from given id.
	Content: valid car json object.
	
	ALLOWED_ROLE: ADMIN
	
## /user/{id}
GET	Retrieve user from given id. If id is empty returns all users.
	Returns json array with user objects.
	
	ALLOWED_ROLE: ALL
	
POST Upload user credentials. 
	Content: valid user json object.
	
	ALLOWED_ROLE: ALL
	
DELETE Remove user from given id.
	Allows a user to delete his own account.
	
	ALLOWED_ROLE: USER
	
PUT	Update user from given id.
	Content: valid user json object.
	
	ALLOWED_ROLE: ADMIN
	
## /reservation/{id}
GET Retrieve all reservations from user.
	Returns all reservations associated with user in a json array.
	
	ALLOWED_ROLE: USER
	
POST Reserve car .
	Content: valid reservation json object.
	
	ALLOWED_ROLE: USER
	
DELETE Remove reservation with given id.

	ALLOWED_ROLE: USER
	
PUT	Update reservation .
	Content: valid reservation json object.
	
	ALLOWED_ROLE: ADMIN

## /locationManager/{Currency}
GET Retrieve all cars sorted by location with calculated price from given currency.
	
	ALLOWED_ROLE: ALL

## /login
POST Login a user with username and password.
	Content: username and password.
	Returns: Authentication Cookie in http-only header.
	
	ALLOWED_ROLE: ALL
	
# JSON Examples 
baseURL/car
```json
[{
	"id": 1,
	"companyId": 1,
	"brand": "Mazda",
	"model": "3 Supersport",
	"available": true,
	"price": 122.0,
	"driveType": "Diesel",
	"image": "mazda.jpg",
	"description": "This is a Mazda"
}, {
	"id": 2,
	"companyId": 2,
	"brand": "Mercedes",
	"model": "A Klasse",
	"available": true,
	"price": 222.0,
	"driveType": "Benzin",
	"image": "porsche.jpg",
	"description": "This is a Mercedes"
}, {
	"id": 3,
	"companyId": 2,
	"brand": "Toyota",
	"model": "A Klasse",
	"available": true,
	"price": 202.0,
	"driveType": "Benzin",
	"image": "toyota.jpg",
	"description": "This is a Toyota"
}]
```

baseURL/location
```json
[{
	"id": 1,
	"name": "location1",
	"latitude": 49.224323,
	"longitude": 14.000485,
	"address": "some Address"
}, {
	"id": 2,
	"name": "location2",
	"latitude": 49.13243,
	"longitude": 14.100485,
	"address": "some Address"
}]
```

baseURL/user
```json
[{
	"id": 1,
	"email": "user@user.com",
	"password": "userPass"
}, {
	"id": 2,
	"email": "admin@admin.com",
	"password": "adminPass"
}]
```

baseURL/reservation
```json
[{
	"id": 1,
	"userId": 1,
	"carId": 1,
	"startDate": "2019-06-07",
	"endDate": "2019-06-09"
}]
```

baseURL/locationManager
```json
[{
	"location": {
		"id": 1,
		"name": "location1",
		"latitude": 49.224323,
		"longitude": 14.000485,
		"address": "some Address"
	},
	"cars": [{
		"id": 1,
		"companyId": 1,
		"brand": "Mazda",
		"model": "3 Supersport",
		"available": true,
		"price": 122.0,
		"driveType": "Diesel",
		"image": "mazda.jpg",
		"description": "This is a Mazda"
	}]
}, {
	"location": {
		"id": 2,
		"name": "location2",
		"latitude": 49.13243,
		"longitude": 14.100485,
		"address": "some Address"
	},
	"cars": [{
		"id": 2,
		"companyId": 2,
		"brand": "Mercedes",
		"model": "A Klasse",
		"available": true,
		"price": 222.0,
		"driveType": "Benzin",
		"image": "porsche.jpg",
		"description": "This is a Mercedes"
	}, {
		"id": 3,
		"companyId": 2,
		"brand": "Toyota",
		"model": "A Klasse",
		"available": true,
		"price": 202.0,
		"driveType": "Benzin",
		"image": "toyota.jpg",
		"description": "This is a Toyota"
	}]
}]
```
	
