# Running the server
Run from intellij with spring-boot:run maven plugin

# Api
Base URL: localhost:8090/api
resources:  api/car
            api/user
            api/location
            api/reservation
            api/locationManager

Every resource has GET, POST, PUT, DELETE Methods
All GET Methods are public
The other methods need authentication

# Authentication
To authenticate send a POST request to api/login with content username=user&password=userPass
Curl call: curl -i -X POST -d username=user -d password=userPass http://localhost:8090/api/login
The server sends back a Cookie which can be used to authenticate for the requests which require authentication

# CORS Filter
The server might not be able to communicate with the WebApp.
If there is a CORS Error just adapt this line "registry.addMapping("/**").allowedOrigins("http://localhost:8080");"
in the Swagger2SpringBoot class with the port the webApp is running on. 

# JSON Examples 
localhost:8090/api/car
```json
[{
	"id": 1,
	"companyId": 1,
	"brand": "Mazda",
	"model": "3 Supersport",
	"available": true,
	"price": 122.0,
	"driveType": "Diesel",
	"image": "mazda.jpg",
	"description": "This is a Mazda"
}, {
	"id": 2,
	"companyId": 2,
	"brand": "Mercedes",
	"model": "A Klasse",
	"available": true,
	"price": 222.0,
	"driveType": "Benzin",
	"image": "porsche.jpg",
	"description": "This is a Mercedes"
}, {
	"id": 3,
	"companyId": 2,
	"brand": "Toyota",
	"model": "A Klasse",
	"available": true,
	"price": 202.0,
	"driveType": "Benzin",
	"image": "toyota.jpg",
	"description": "This is a Toyota"
}]
```

localhost:8090/api/location
```json
[{
	"id": 1,
	"name": "location1",
	"latitude": 49.224323,
	"longitude": 14.000485,
	"address": "some Address"
}, {
	"id": 2,
	"name": "location2",
	"latitude": 49.13243,
	"longitude": 14.100485,
	"address": "some Address"
}]
```

localhost:8090/api/user
```json
[{
	"id": 1,
	"email": "user@user.com",
	"password": "userPass"
}, {
	"id": 2,
	"email": "admin@admin.com",
	"password": "adminPass"
}]
```

localhost:8090/api/reservation
```json
[{
	"id": 1,
	"userId": 1,
	"carId": 1,
	"startDate": "2019-06-07",
	"endDate": "2019-06-09"
}]
```

localhost:8090/api/locationManager
```json
[{
	"location": {
		"id": 1,
		"name": "location1",
		"latitude": 49.224323,
		"longitude": 14.000485,
		"address": "some Address"
	},
	"cars": [{
		"id": 1,
		"companyId": 1,
		"brand": "Mazda",
		"model": "3 Supersport",
		"available": true,
		"price": 122.0,
		"driveType": "Diesel",
		"image": "mazda.jpg",
		"description": "This is a Mazda"
	}]
}, {
	"location": {
		"id": 2,
		"name": "location2",
		"latitude": 49.13243,
		"longitude": 14.100485,
		"address": "some Address"
	},
	"cars": [{
		"id": 2,
		"companyId": 2,
		"brand": "Mercedes",
		"model": "A Klasse",
		"available": true,
		"price": 222.0,
		"driveType": "Benzin",
		"image": "porsche.jpg",
		"description": "This is a Mercedes"
	}, {
		"id": 3,
		"companyId": 2,
		"brand": "Toyota",
		"model": "A Klasse",
		"available": true,
		"price": 202.0,
		"driveType": "Benzin",
		"image": "toyota.jpg",
		"description": "This is a Toyota"
	}]
}]
```