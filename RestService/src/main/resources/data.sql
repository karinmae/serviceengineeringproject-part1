insert into location (address, latitude, longitude, name) values
('Some Address', 48.005021, 16.231769, 'Baden bei Wien'),
('Some Address', 47.805384, 13.051749, 'Salzburg Stadt');
insert into car(available, brand, location_id, description, drive_type, image, model, price) values
(true, 'Mazda', 1, 'This is a Mazda', 'Diesel', 'mazda.jpg', '3 Supersport', 122.0),
(true, 'Porsche', 1, 'This is a Porsche', 'Benzin', 'porsche.jpg', '911', 222.0),
(true, 'Toyota', 2, 'This is a Toyota', 'Benzin', 'toyota.jpg', '3 Supersport', 202.0);