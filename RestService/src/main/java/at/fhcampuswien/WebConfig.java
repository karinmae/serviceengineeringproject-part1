package at.fhcampuswien;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
public class WebConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserService userService;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    RestAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    SavedRequestAwareAuthenticationSuccessHandler successHandler;

    private SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .authorizeRequests()
                .antMatchers("/h2-console").permitAll()
                .antMatchers(HttpMethod.GET, "/car").permitAll()
                .antMatchers(HttpMethod.POST, "/car").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/car").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/car").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/location").permitAll()
                .antMatchers(HttpMethod.POST, "/location").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.PUT, "/location").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/location").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/reservation").hasAuthority("USER")
                .antMatchers(HttpMethod.POST, "/reservation").hasAuthority("USER")
                .antMatchers(HttpMethod.OPTIONS, "/reservation").permitAll()
                .antMatchers(HttpMethod.PUT, "/reservation").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/reservation").hasAuthority("USER")
                .antMatchers(HttpMethod.GET, "/user").permitAll()
                .antMatchers(HttpMethod.POST, "/user").permitAll()
                .antMatchers(HttpMethod.PUT, "/user").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/user").permitAll()
                .antMatchers("/locationManager").permitAll()
                .and()
                .headers().frameOptions().disable()
                .and()
                .formLogin()
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .and()
                .logout();
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userService);
        provider.setPasswordEncoder(encoder());
        return provider;
    }
}