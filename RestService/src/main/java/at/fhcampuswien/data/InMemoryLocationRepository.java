package at.fhcampuswien.data;

import at.fhcampuswien.model.Location;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InMemoryLocationRepository implements LocationRepository {

    private Map<Integer, Location> locations;

    public InMemoryLocationRepository() {
        this.locations = new HashMap<>();
        createInitialLocations();
    }

    @Override
    public Location findById(Integer id) {
        return locations.get(id);
    }

    public List<Location> getAll() {
        return new ArrayList<>(locations.values());
    }

    @Override
    public void delete(Integer id) {
        locations.remove(id);
    }

    public void store(final Location location) {
        locations.put(location.getId(), location);
    }

    @Override
    public void update(Location location) {
        locations.replace(location.getId(), location);
    }

    private void createInitialLocations() {
        Location location = new Location().address("some Address").id(1).longitude(16.231769).latitude(48.005021).name("Baden bei Wien");
        Location location2 = new Location().address("some Address").id(2).longitude(13.051749).latitude(47.805384).name("Salzburg Stadt");
        locations.put(location.getId(), location);
        locations.put(location2.getId(), location2);
    }
}
