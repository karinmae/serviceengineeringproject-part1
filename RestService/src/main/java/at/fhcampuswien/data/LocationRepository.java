package at.fhcampuswien.data;

import at.fhcampuswien.model.Location;

public interface LocationRepository extends ModelRepository<Location> {
}
