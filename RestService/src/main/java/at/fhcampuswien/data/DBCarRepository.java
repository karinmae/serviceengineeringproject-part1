package at.fhcampuswien.data;

import at.fhcampuswien.model.Car;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DBCarRepository implements CarRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    @Override
    public List<Car> findByLocation(Integer locationId) {
        return getAll().stream().filter(car -> car.getCompanyId().equals(locationId)).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public Car findById(Integer id) {
        return entityManager.find(Car.class, id);
    }

    @Transactional
    @Override
    public List<Car> getAll() {
        Query query = entityManager.createQuery("SELECT e FROM Car e");
        List<Car> resultList = (List<Car>) query.getResultList();
        return resultList;
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        Car car = findById(id);
        entityManager.remove(car);
    }

    @Transactional
    @Override
    public void store(Car car) {
        entityManager.persist(car);
    }

    @Override
    public void update(Car model) {

    }
}
