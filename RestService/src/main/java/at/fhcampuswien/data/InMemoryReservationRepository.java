package at.fhcampuswien.data;

import at.fhcampuswien.model.Reservation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InMemoryReservationRepository implements ReservationRepository {

    private Map<Integer, Reservation> reservations;

    public InMemoryReservationRepository() {
        this.reservations = new HashMap<>();
        init();
    }

    @Override
    public Reservation findById(Integer id) {
        return reservations.get(id);
    }

    @Override
    public List<Reservation> getAll() {
        return new ArrayList<>(reservations.values());
    }

    @Override
    public void delete(Integer id) {
        reservations.remove(id);
    }

    @Override
    public void store(Reservation reservation) {
        reservations.put(reservation.getId(), reservation);
    }

    @Override
    public void update(Reservation reservation) {
        reservations.replace(reservation.getId(), reservation);
    }

    private void init() {
        Reservation reservation1 = new Reservation();
        reservation1.setCarId(1);
        reservation1.setId(1);
        reservation1.setUserId(1);
        reservation1.setStartDate("2019-06-07");
        reservation1.setEndDate("2019-06-09");
        reservations.put(reservation1.getId(), reservation1);
    }
}
