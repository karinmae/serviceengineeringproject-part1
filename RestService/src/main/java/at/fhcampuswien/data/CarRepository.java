package at.fhcampuswien.data;

import at.fhcampuswien.model.Car;

import java.util.List;

public interface CarRepository extends ModelRepository<Car> {
    List<Car> findByLocation(Integer locationId);
}
