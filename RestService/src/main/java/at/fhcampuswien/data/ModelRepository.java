package at.fhcampuswien.data;

import java.util.List;

public interface ModelRepository<T> {
    T findById(Integer id);
    List<T> getAll();
    void delete(Integer id);
    void store(T model);
    void update(T model);
}
