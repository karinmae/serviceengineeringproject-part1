package at.fhcampuswien.data;

import at.fhcampuswien.model.Car;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class InMemoryCarRepository implements CarRepository {

    private Map<Integer, Car> carMap;

    public InMemoryCarRepository() {
        this.carMap = new HashMap<>();
        createInitialCars();
    }

    @Override
    public Car findById(final Integer carId) {
        return carMap.get(carId);
    }


    @Override
    public List<Car> findByLocation(final Integer locationId) {
        return carMap.values().stream().filter(car -> car.getCompanyId().equals(locationId)).collect(Collectors.toList());
    }


    @Override
    public List<Car> getAll() {
        System.out.println(carMap.size());
        return new ArrayList<>(carMap.values());
    }

    @Override
    public void delete(Integer id) {
        carMap.remove(id);
    }

    @Override
    public void store(final Car car) {
        System.out.println(car);
        System.out.println(carMap.size());
        carMap.put(car.getId(), car);
        System.out.println(carMap.size());
    }

    @Override
    public void update(Car car) {
        carMap.replace(car.getId(), car);
    }

    private void createInitialCars() {
        Car car1 = new Car().available(true)
                .brand("Mazda")
                .companyId(1)
                .description("This is a Mazda")
                .model("3 Supersport")
                .driveType("Diesel")
                .id(1)
                .price(122.00)
                .image("mazda.jpg");
        store(car1);
        Car car2 = new Car().available(true)
                .brand("Mercedes")
                .companyId(2)
                .description("This is a Mercedes")
                .model("A Klasse")
                .driveType("Benzin")
                .id(2)
                .price(222.00)
                .image("porsche.jpg");
        store(car2);
        Car car3 = new Car().available(true)
                .brand("Toyota")
                .companyId(2)
                .description("This is a Toyota")
                .model("A Klasse")
                .driveType("Benzin")
                .id(3)
                .price(202.00)
                .image("toyota.jpg");
        store(car3);
    }
}
