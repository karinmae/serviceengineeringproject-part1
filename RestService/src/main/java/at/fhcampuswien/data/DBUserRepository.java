package at.fhcampuswien.data;

import at.fhcampuswien.model.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class DBUserRepository implements UserRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    @Override
    public User findById(Integer id) {
        return entityManager.find(User.class, id);
    }

    @Transactional
    @Override
    public User findByName(String username) {
        List<User> users = getAll();
        return users.stream().filter(user -> user.getEmail().equalsIgnoreCase(username)).findFirst().orElse(null);
    }
    @Transactional
    @Override
    public List<User> getAll() {
        Query query = entityManager.createQuery("SELECT e FROM User e");
        List<User> resultList = (List<User>) query.getResultList();
        return resultList;
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        User user = entityManager.find(User.class, id);
        entityManager.remove(user);
    }

    @Transactional
    @Override
    public void store(User model) {
        entityManager.persist(model);
    }

    @Override
    public void update(User model) {
    }
}
