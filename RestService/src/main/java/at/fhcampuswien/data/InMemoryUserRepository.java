package at.fhcampuswien.data;

import at.fhcampuswien.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class InMemoryUserRepository implements UserRepository {

    private Map<Integer, User> users;

    public InMemoryUserRepository() {
        users = new HashMap<>();
        init();
    }

    @Override
    public User findById(Integer id) {
        return users.get(id);
    }

    @Override
    public List<User> getAll() {
        return new ArrayList<>(users.values());
    }

    @Override
    public void delete(Integer id) {
        users.remove(id);
    }

    @Override
    public void store(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public void update(User user) {
        users.replace(user.getId(), user);
    }

    private void init() {
        User user1 = new User();
        user1.setId(1);
        user1.setEmail("user@user.com");
        user1.setPassword("userPass");
        User user2 = new User();
        user2.setId(2);
        user2.setEmail("admin@admin.com");
        user2.setPassword("adminPass");
        store(user1);
        store(user2);
    }

    @Override
    public User findByName(String name) {
        return null;
    }
}
