package at.fhcampuswien.data;

import at.fhcampuswien.model.Location;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class DBLocationRepository implements LocationRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    @Override
    public Location findById(Integer id) {
        return entityManager.find(Location.class, id);
    }

    @Transactional
    @Override
    public List<Location> getAll() {
        Query query = entityManager.createQuery("SELECT e FROM Location e");
        List<Location> resultList = (List<Location>) query.getResultList();
        return resultList;
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        Location location = findById(id);
        entityManager.remove(location);
    }

    @Transactional
    @Override
    public void store(Location model) {
        entityManager.persist(model);
    }

    @Override
    public void update(Location model) {

    }
}
