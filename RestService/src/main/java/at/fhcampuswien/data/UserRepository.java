package at.fhcampuswien.data;

import at.fhcampuswien.model.User;

public interface UserRepository extends ModelRepository<User>{
    public User findByName(String name);
}
