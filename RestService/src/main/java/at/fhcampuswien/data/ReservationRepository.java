package at.fhcampuswien.data;

import at.fhcampuswien.model.Reservation;

public interface ReservationRepository extends ModelRepository<Reservation> {
}
