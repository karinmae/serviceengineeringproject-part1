package at.fhcampuswien.data;

import at.fhcampuswien.model.Reservation;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class DBReservationRepository implements ReservationRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    @Override
    public Reservation findById(Integer id) {
        return entityManager.find(Reservation.class, id);
    }

    @Transactional
    @Override
    public List<Reservation> getAll() {
        Query query = entityManager.createQuery("SELECT e FROM Reservation e");
        List<Reservation> resultList = (List<Reservation>) query.getResultList();
        return resultList;
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        Reservation reservation = findById(id);
        entityManager.remove(reservation);
    }

    @Transactional
    @Override
    public void store(Reservation model) {
        entityManager.persist(model);
    }

    @Override
    public void update(Reservation model) {
    }
}
