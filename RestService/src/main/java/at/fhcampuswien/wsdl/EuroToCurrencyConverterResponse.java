
package at.fhcampuswien.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EuroToCurrencyConverterResult" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "euroToCurrencyConverterResult"
})
@XmlRootElement(name = "EuroToCurrencyConverterResponse")
public class EuroToCurrencyConverterResponse {

    @XmlElement(name = "EuroToCurrencyConverterResult")
    protected float euroToCurrencyConverterResult;

    /**
     * Gets the value of the euroToCurrencyConverterResult property.
     * 
     */
    public float getEuroToCurrencyConverterResult() {
        return euroToCurrencyConverterResult;
    }

    /**
     * Sets the value of the euroToCurrencyConverterResult property.
     * 
     */
    public void setEuroToCurrencyConverterResult(float value) {
        this.euroToCurrencyConverterResult = value;
    }

}
