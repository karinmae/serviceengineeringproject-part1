
package at.fhcampuswien.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the at.fhcampuswien.currency package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: at.fhcampuswien.currency
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EuroToCurrencyConverterResponse }
     * 
     */
    public EuroToCurrencyConverterResponse createEuroToCurrencyConverterResponse() {
        return new EuroToCurrencyConverterResponse();
    }

    /**
     * Create an instance of {@link GetCrossRateResponse }
     * 
     */
    public GetCrossRateResponse createGetCrossRateResponse() {
        return new GetCrossRateResponse();
    }

    /**
     * Create an instance of {@link EuroToCurrencyConverter }
     * 
     */
    public EuroToCurrencyConverter createEuroToCurrencyConverter() {
        return new EuroToCurrencyConverter();
    }

    /**
     * Create an instance of {@link GetCrossRate }
     * 
     */
    public GetCrossRate createGetCrossRate() {
        return new GetCrossRate();
    }

}
