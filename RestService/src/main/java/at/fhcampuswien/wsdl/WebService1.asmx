<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://tempuri.org/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/">
      <s:element name="EuroToCurrencyConverter">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="code" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="euro_price" type="s:float" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="EuroToCurrencyConverterResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="EuroToCurrencyConverterResult" type="s:float" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetCrossRate">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="from_code" type="s:string" />
            <s:element minOccurs="0" maxOccurs="1" name="to_code" type="s:string" />
            <s:element minOccurs="1" maxOccurs="1" name="price" type="s:float" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetCrossRateResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="1" maxOccurs="1" name="GetCrossRateResult" type="s:float" />
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="EuroToCurrencyConverterSoapIn">
    <wsdl:part name="parameters" element="tns:EuroToCurrencyConverter" />
  </wsdl:message>
  <wsdl:message name="EuroToCurrencyConverterSoapOut">
    <wsdl:part name="parameters" element="tns:EuroToCurrencyConverterResponse" />
  </wsdl:message>
  <wsdl:message name="GetCrossRateSoapIn">
    <wsdl:part name="parameters" element="tns:GetCrossRate" />
  </wsdl:message>
  <wsdl:message name="GetCrossRateSoapOut">
    <wsdl:part name="parameters" element="tns:GetCrossRateResponse" />
  </wsdl:message>
  <wsdl:portType name="WebService1Soap">
    <wsdl:operation name="EuroToCurrencyConverter">
      <wsdl:input message="tns:EuroToCurrencyConverterSoapIn" />
      <wsdl:output message="tns:EuroToCurrencyConverterSoapOut" />
    </wsdl:operation>
    <wsdl:operation name="GetCrossRate">
      <wsdl:input message="tns:GetCrossRateSoapIn" />
      <wsdl:output message="tns:GetCrossRateSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="WebService1Soap" type="tns:WebService1Soap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="EuroToCurrencyConverter">
      <soap:operation soapAction="http://tempuri.org/EuroToCurrencyConverter" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCrossRate">
      <soap:operation soapAction="http://tempuri.org/GetCrossRate" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="WebService1Soap12" type="tns:WebService1Soap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="EuroToCurrencyConverter">
      <soap12:operation soapAction="http://tempuri.org/EuroToCurrencyConverter" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCrossRate">
      <soap12:operation soapAction="http://tempuri.org/GetCrossRate" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="WebService1">
    <wsdl:port name="WebService1Soap" binding="tns:WebService1Soap">
      <soap:address location="https://aspwebcurrencyserviceengineering.azurewebsites.net/WebService1.asmx" />
    </wsdl:port>
    <wsdl:port name="WebService1Soap12" binding="tns:WebService1Soap12">
      <soap12:address location="https://aspwebcurrencyserviceengineering.azurewebsites.net/WebService1.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>