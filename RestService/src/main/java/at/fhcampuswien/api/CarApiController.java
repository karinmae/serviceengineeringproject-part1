package at.fhcampuswien.api;

import at.fhcampuswien.data.DBCarRepository;
import at.fhcampuswien.data.InMemoryCarRepository;
import at.fhcampuswien.model.Car;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-04-19T15:28:22.691Z[GMT]")
@Controller
@CrossOrigin
public class CarApiController implements CarApi {

    private static final Logger log = LoggerFactory.getLogger(CarApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private DBCarRepository repository;

    @org.springframework.beans.factory.annotation.Autowired
    public CarApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<List<Car>> searchCars(@ApiParam(value = "get a specific car by its id") @Valid @PathVariable(value = "id", required = false) Integer id) {
        String accept = request.getHeader("Accept");
        List<Car> cars;
        if (id != null) {
            cars = Collections.singletonList(repository.findById(id));
        } else {
            cars = repository.getAll();
        }
        log.info("# Received GET Request for cars");
        return new ResponseEntity<List<Car>>(cars, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> addCar(@RequestBody Car body) {
        repository.store(body);
        log.info("# Received POST Request for cars");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteCar(@PathVariable(value = "id", required = true) Integer id) {
        repository.delete(id);
        log.info("# Received DELETE Request for cars");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateCar(@RequestBody Car body) {
        repository.update(body);
        log.info("# Received PUT Request for cars");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
