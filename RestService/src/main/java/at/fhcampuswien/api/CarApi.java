/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.7).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package at.fhcampuswien.api;

import at.fhcampuswien.model.Car;
import at.fhcampuswien.model.User;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-04-19T15:28:22.691Z[GMT]")
@Api(value = "car", description = "the car API")
public interface CarApi {

    @ApiOperation(value = "search for cars", nickname = "searchCars", notes = "Search for cars in the database", response = Car.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "search results matching criteria", response = Car.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "bad input parameter")})
    @RequestMapping(path = {"/car/{id}","/car"},
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<Car>> searchCars(@ApiParam(value = "get a specific car by its id") @Valid @PathVariable(value = "id", required = false) Integer id);

    @RequestMapping(value = "/car",
            consumes = { "application/json" },
            method = RequestMethod.POST)
    ResponseEntity<Void> addCar(@ApiParam(value = "Inventory item to add"  )  @Valid @RequestBody Car body);

    @RequestMapping(value = "/car/{id}", method = RequestMethod.DELETE)
    ResponseEntity<Void> deleteCar(@Valid @PathVariable(value = "id", required = true) Integer id);

    @RequestMapping(value = "/car", method = RequestMethod.PUT, consumes = {"application/json"})
    ResponseEntity<Void> updateCar(@Valid @RequestBody Car body);

}
