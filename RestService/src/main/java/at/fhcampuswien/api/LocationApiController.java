package at.fhcampuswien.api;

import at.fhcampuswien.data.DBLocationRepository;
import at.fhcampuswien.data.InMemoryLocationRepository;
import at.fhcampuswien.data.LocationRepository;
import at.fhcampuswien.model.Car;
import at.fhcampuswien.model.Location;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-04-19T15:28:22.691Z[GMT]")
@Controller
@CrossOrigin
public class LocationApiController implements LocationApi {

    private static final Logger log = LoggerFactory.getLogger(LocationApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private DBLocationRepository repository;

    @org.springframework.beans.factory.annotation.Autowired
    public LocationApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<List<Location>> searchLocation(@ApiParam(value = "get a specific car by its id") @Valid @RequestParam(value = "id", required = false) Integer id) {
        String accept = request.getHeader("Accept");
        List<Location> locations;
        if (id != null) {
            locations = Collections.singletonList(repository.findById(id));
        } else {
            locations = repository.getAll();
        }
        log.info("#Received GET Request for locations");
        return new ResponseEntity<List<Location>>(locations, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> addLocation(@RequestBody Location body) {
        repository.store(body);
        log.info("#Received POST Request for locations");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> updateLocation(@RequestBody Location body) {
        repository.update(body);
        log.info("#Received PUT Request for locations");
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @Override
    public ResponseEntity<Void> deleteLocation(@PathVariable Integer id) {
        repository.delete(id);
        log.info("#Received Delete Request for locations");
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
