package at.fhcampuswien.api;

import at.fhcampuswien.model.DetailedLocation;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Api(value = "locationManager", description = "the location manager")
public interface LocationManagerApi {
    @RequestMapping(path = {"/locationManager/{CR}", "/locationManager"},
            produces = { "application/json" },
            method = RequestMethod.GET)
    ResponseEntity<List<DetailedLocation>> getAll(@PathVariable(value = "CR", required = false) String currency);

}
