package at.fhcampuswien.api;

import at.fhcampuswien.data.DBCarRepository;
import at.fhcampuswien.data.DBLocationRepository;
import at.fhcampuswien.model.Car;
import at.fhcampuswien.model.DetailedLocation;
import at.fhcampuswien.model.InternalCar;
import at.fhcampuswien.model.Location;
import at.fhcampuswien.wsdl.GetCrossRate;
import at.fhcampuswien.wsdl.WebService1;
import at.fhcampuswien.wsdl.WebService1Soap;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@CrossOrigin
public class LocationManagerApiController implements LocationManagerApi {
    private static final Logger log = LoggerFactory.getLogger(LocationManagerApiController.class);
    @Autowired
    private DBCarRepository carRepository;
    @Autowired
    private DBLocationRepository locationRepository;

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private final WebService1Soap currencyService;

    @Autowired
    public LocationManagerApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
        currencyService = new WebService1().getWebService1Soap();
    }

    @Override
    public ResponseEntity<List<DetailedLocation>> getAll(@PathVariable(value = "CR", required = false) String currency) {
        String accept = request.getHeader("Accept");
        List<DetailedLocation> detailedLocations = getLocationCars(currency);
        log.info("#Received GET Request for locationManager");
        return new ResponseEntity<>(detailedLocations, HttpStatus.OK);
    }

    private List<DetailedLocation> getLocationCars(String currency) {
        List<Location> locations = locationRepository.getAll();
        List<DetailedLocation> detailedLocations = new ArrayList<>();
        for (Location location : locations) {
            List<InternalCar> cars = new ArrayList<>();
            for (Car car : carRepository.findByLocation(location.getId())) {
                cars.add(new InternalCar(car));
            }
                for (InternalCar car : cars) {
                    double oldPrice = car.getPrice();
                    car.setPrice((double)getPriceFromWsdl(oldPrice, currency));
                }
            DetailedLocation c = new DetailedLocation(location, cars);
            detailedLocations.add(c);
        }

        return detailedLocations;
    }

    private float getPriceFromWsdl(double initialPrice, String currency) {
        float crossRate = currencyService.getCrossRate("USD", currency, (float) initialPrice);
        return Math.round(crossRate);
    }

}

