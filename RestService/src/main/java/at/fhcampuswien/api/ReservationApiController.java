package at.fhcampuswien.api;

import at.fhcampuswien.data.*;
import at.fhcampuswien.model.Reservation;
import at.fhcampuswien.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-04-19T15:28:22.691Z[GMT]")
@Controller
@CrossOrigin
public class ReservationApiController implements ReservationApi {

    private static final Logger log = LoggerFactory.getLogger(ReservationApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private DBReservationRepository repository;

    @Autowired
    private DBUserRepository userRepository;

    @Autowired
    private DBCarRepository carRepository;

    @org.springframework.beans.factory.annotation.Autowired
    public ReservationApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Transactional
    public ResponseEntity<Void> addReservation(@ApiParam(value = "Reservation") @Valid @RequestBody Reservation reservation, Authentication authentication) {
        String accept = request.getHeader("Accept");
        reservation.setUserId(getIdFromuser(authentication));
        repository.store(reservation);
        if(carRepository.findById(reservation.getCarId()).getAvailable() == false){
            log.info("#Received POST Request for already reserved car");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            carRepository.findById(reservation.getCarId()).setAvailable(false);
        }
        log.info("#Received POST Request for reservations");
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<List<Reservation>> searchReservations(@NotNull @ApiParam(value = "pass an optional search string for looking up inventory", required = false) @Valid @RequestParam(value = "id", required = false) Integer id, Authentication authentication) {
        String accept = request.getHeader("Accept");
        Integer userId = getIdFromuser(authentication);
        List<Reservation> reservations = repository.getAll();
        List<Reservation> userReservations = reservations.stream().filter(reservation -> reservation.getUserId().equals(userId)).collect(Collectors.toList());
        log.info("#Received GET Request for reservations");
        return new ResponseEntity<List<Reservation>>(userReservations, HttpStatus.OK);
    }

    @Override
    @Transactional
    public ResponseEntity<Void> deleteReservation(@PathVariable Integer id) {
        Reservation reservation = repository.findById(id);
        carRepository.findById(reservation.getCarId()).setAvailable(true);
        repository.delete(id);
        log.info("#Received DELETE Request for reservations");
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    private Integer getIdFromuser(Authentication authentication) {
        User user = userRepository.findByName(authentication.getName());
        return user != null ? user.getId() : null;
    }

}
