package at.fhcampuswien.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Car
 */
@Validated
@Entity
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2019-04-19T15:28:22.691Z[GMT]")
public class Car   {
  @JsonProperty("id")
  @Id
  @GeneratedValue
  private Integer id = null;

  @ManyToOne(targetEntity = Location.class, fetch = FetchType.EAGER)
  @JoinColumn(name = "location_id", insertable = false, updatable = false)
  @JsonIgnore
  private Location location;

  @JsonProperty("companyId")
  @Column(name = "location_id")
  private Integer companyId = null;

  @JsonProperty("brand")
  private String brand = null;

  @JsonProperty("model")
  private String model = null;

  @JsonProperty("available")
  private Boolean available = null;

  @JsonProperty("price")
  private Double price = null;

  @JsonProperty("driveType")
  private String driveType = null;

  @JsonProperty("image")
  private String image = null;

  @JsonProperty("description")
  private String description = null;

  public Car id(Integer id) {
    this.id = id;
    return this;
  }

  public Car() {
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "1", required = true, value = "")

  @Valid
  public Integer getId() {
    return id;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Boolean getAvailable() {
    return available;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Car companyId(Integer companyId) {
    this.companyId = companyId;
    return this;
  }

  /**
   * Get companyId
   * @return companyId
  **/
  @ApiModelProperty(example = "1", required = true, value = "")
  @NotNull

  @Valid
  public Integer getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Integer companyId) {
    this.companyId = companyId;
  }

  public Car brand(String brand) {
    this.brand = brand;
    return this;
  }

  /**
   * Get brand
   * @return brand
  **/
  @ApiModelProperty(example = "Mazda", required = true, value = "")
  @NotNull

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public Car model(String model) {
    this.model = model;
    return this;
  }

  /**
   * Get model
   * @return model
  **/
  @ApiModelProperty(example = "3 Sportedition", required = true, value = "")
  @NotNull

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public Car available(Boolean available) {
    this.available = available;
    return this;
  }

  /**
   * Get available
   * @return available
  **/
  @ApiModelProperty(example = "true", required = true, value = "")
  @NotNull

  public Boolean isAvailable() {
    return available;
  }

  public void setAvailable(Boolean available) {
    this.available = available;
  }

  public Car price(Double price) {
    this.price = price;
    return this;
  }

  /**
   * Get price
   * @return price
  **/
  @ApiModelProperty(example = "120", required = true, value = "")
  @NotNull

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Car driveType(String driveType) {
    this.driveType = driveType;
    return this;
  }

  /**
   * Get driveType
   * @return driveType
  **/
  @ApiModelProperty(example = "Diesel", required = true, value = "")
  @NotNull

  public String getDriveType() {
    return driveType;
  }

  public void setDriveType(String driveType) {
    this.driveType = driveType;
  }

  public Car image(String image) {
    this.image = image;
    return this;
  }

  /**
   * Get image
   * @return image
  **/
  @ApiModelProperty(example = "mazda.jpg", value = "")

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Car description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "This is a description of a car", value = "")

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Car car = (Car) o;
    return Objects.equals(this.id, car.id) &&
        Objects.equals(this.companyId, car.companyId) &&
        Objects.equals(this.brand, car.brand) &&
        Objects.equals(this.model, car.model) &&
        Objects.equals(this.available, car.available) &&
        Objects.equals(this.price, car.price) &&
        Objects.equals(this.driveType, car.driveType) &&
        Objects.equals(this.image, car.image) &&
        Objects.equals(this.description, car.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, companyId, brand, model, available, price, driveType, image, description);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Car {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
    sb.append("    brand: ").append(toIndentedString(brand)).append("\n");
    sb.append("    model: ").append(toIndentedString(model)).append("\n");
    sb.append("    available: ").append(toIndentedString(available)).append("\n");
    sb.append("    price: ").append(toIndentedString(price)).append("\n");
    sb.append("    driveType: ").append(toIndentedString(driveType)).append("\n");
    sb.append("    image: ").append(toIndentedString(image)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
