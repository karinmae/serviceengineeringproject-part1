package at.fhcampuswien.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DetailedLocation {
    @JsonProperty("location")
    private Location location;
    @JsonProperty("cars")
    private List<InternalCar> cars;

    public DetailedLocation(Location location, List<InternalCar> cars) {
        this.location = location;
        this.cars = cars;
    }

    public DetailedLocation() {
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<InternalCar> getCars() {
        return cars;
    }

    public void setCars(List<InternalCar> cars) {
        this.cars = cars;
    }
}
