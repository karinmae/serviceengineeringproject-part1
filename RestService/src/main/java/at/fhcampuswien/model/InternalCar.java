package at.fhcampuswien.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InternalCar {

    public InternalCar(Car car) {
        this.id = car.getId();
        this.companyId = car.getCompanyId();
        this.available = car.getAvailable();
        this.brand = car.getBrand();
        this.description = car.getDescription();
        this.driveType = car.getDriveType();
        this.image = car.getImage();
        this.price = car.getPrice();
        this.model = car.getModel();
    }

    @JsonProperty("id")
    private Integer id = null;

    @JsonProperty("companyId")
    private Integer companyId = null;

    @JsonProperty("brand")
    private String brand = null;

    @JsonProperty("model")
    private String model = null;

    @JsonProperty("available")
    private Boolean available = null;

    @JsonProperty("price")
    private Double price = null;

    @JsonProperty("driveType")
    private String driveType = null;

    @JsonProperty("image")
    private String image = null;

    @JsonProperty("description")
    private String description = null;

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice() {
        return price;
    }
}
