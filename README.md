# Car Rental Application and Web Services

This project contains of:

	* Car Rental service (Java REST service)
	* Currency Converter service (.NET SOAP/WSDL service)
	* Web application (interacting with google maps web services and Car Rental service)


## Group 4:

	* Thomas Dutka
	* Leon Freudenthaler
	* Karin Männersdorfer
	* Florian Niszl
	* Michael Trautner

