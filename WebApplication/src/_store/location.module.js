import { locationService } from '../_services';

const locationData = [];
const state = {
    all: {}
};

const actions = {
    getAll({ dispatch, commit }, currency) {
        commit('getAllLocationsRequest');

        locationService.getAll(currency)
            .then(
                locations => commit('getAllSuccess', locations),
                error => {
                    commit('getAllFailure', error.message);
                    dispatch('alert/error', error.message, { root: true });
                }
            );
    },

    updateLocationsAction: function ({commit}, locations) {
        commit('updateLocations', locations)
    }
};

const mutations = {
    updateLocations(state, locations) {
        state.locations = locations;
    },
    getAllSuccess(state, locations) {
        state.all = { items: locations };
    },
    getAllFailure(state, error) {
        state.all = { error };
    },
    getAllLocationsRequest() {
        state.all = { loading: true };
    },
    locationSuccess(locations) {
        state.locations = locations;
    },
    locationFailure(state) {
        state.locations = [];
    }
};

export const locations = {
    namespaced: true,
    state,
    //getters,
    actions,
    mutations
};