import Axios from 'axios'
import { config } from '../_helpers';

export const locationService = {
    create,
    getAll,
    getById: get,
    update,
    delete: _delete
};

function getAll(currency) {
  if(currency === undefined){
    currency = 'USD';
  }
  return Axios.get(`${config.apiUrl}/locationManager/${currency}`, config.apiConfig)
              .then(handleResponse)
              .then(data => {
                return data;
              })
              .catch(error => {
                if (!error.response) {
                  return Promise.reject(error);
                } else {
                  console.error(error.response.data.message);
                }
              })

              
}
  
function get(id) {
  return Axios.get(`${config.apiUrl}/${id}`);
}
  
function create(data) {
  return Axios.post(config.apiUrl, data);
}
  
function update(id, data) {
  return Axios.put(`${config.apiUrl}/${id}`, data);
}
  
function _delete(id) {
  return Axios.delete(`${config.apiUrl}/${id}`);
}

function handleResponse(response) {
  if(response.status === 200){
    return response.data;
  }else{
    return Promise.reject(error);
  }
}

