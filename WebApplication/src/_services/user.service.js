import { config } from '../_helpers';
import { authHeader } from '../_helpers';
import Axios from 'axios'

export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    delete: _delete
};

function login(username, password) {
    let u_body = 'username=' + username + '&password=' + password;
    let u_user = {}

    return Axios.post(`${config.apiUrl}/login`, u_body, {
                        withCredentials: true, 
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    })
                .then(response =>{
                    if(response.status === 200){
                        u_user = {
                            username: username,
                            firstName: '',
                            lastName: '',
                            token: 'fake-jwt-token'
                        }
                        localStorage.setItem('user', JSON.stringify(u_user));
                    }
                    return u_user;
                })
                .catch(error =>{
                    return Promise.reject(error);
                })

}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function register(user) {
    user.email = user.username;
    return Axios.post(`${config.apiUrl}/user`, user)
                .then(handleResponse)
                .then(
                response =>{
                    if(response.status === 200){
                        return response.data;
                    }
                },
                error =>{
                    return Promise.reject('Username not available');
                })
}

function getAll() {
    return Axios.get(`${config.apiUrl}/user`)
                .then(handleResponse)
                .then(data => {
                    return data;
                })
}


function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions)
            .then(handleResponse)
}


// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return Axios.delete(`${config.apiUrl}/${id}`);
}

function handleResponse(response) {
    if(response.status === 200){
      return response.data;
    }else{
      return Promise.reject(error);
    }
}