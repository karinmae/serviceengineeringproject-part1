import Axios from 'axios'
import { config } from '../_helpers';

export const reservationService = {
    bookCar,
    returnCar,
    getBookedCarsFromUser
};

function bookCar(data) {
  return Axios.post(`${config.apiUrl}/reservation`, data, {withCredentials: true});
}

function returnCar(reservationID) {
  return Axios.delete(`${config.apiUrl}/reservation/${reservationID}`, {withCredentials: true});
}

function getBookedCarsFromUser() {
  return Axios.get(`${config.apiUrl}/reservation`, {withCredentials: true});
}


