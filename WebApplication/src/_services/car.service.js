import Axios from 'axios'
import { config } from '../_helpers';

export const carService = {
  getCars
};

function getCars() {
  return Axios.get(`${config.apiUrl}/car`, {withCredentials: true});
}

  