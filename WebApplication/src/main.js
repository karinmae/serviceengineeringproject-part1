import Vue from 'vue'
import App from './App.vue'
import VeeValidate from 'vee-validate';
import Toasted from 'vue-toasted';
import { store } from './_store';
import { router } from './_helpers';
import "./_helpers/toaster";
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

window.$ = window.jQuery = require('jquery');

Vue.config.productionTip = false
Vue.use(VeeValidate);


// setup fake backend
import { configureFakeBackend } from './_helpers';
configureFakeBackend();

export const vm = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
