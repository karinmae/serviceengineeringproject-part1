import Vue from 'vue';
import Router from 'vue-router';

import Home from '../home/Home.vue'
import Account from '../account/Account.vue'
import Cars from '../cars/Cars'
import CarDetail from '../carDetail/CarDetail'
import LoginPage from '../login/LoginPage'
import RegisterPage from '../register/RegisterPage'


Vue.use(Router)

export const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', component: Home },
    { path: '/account', component: Account },
    { path: '/cars', component: Cars },
    { path: '/login', component: LoginPage },
    { path: '/register', component: RegisterPage },
    { path: '/carDetail/:carID', name:'carDetail', component: CarDetail, props: (route) => ({...route.params}) },

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
});

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page

  //public pages -> allowed to see if not logged in
  const publicPages = ['/login', '/register', '/', '/cars'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next('/login');
  }

  next();
})
