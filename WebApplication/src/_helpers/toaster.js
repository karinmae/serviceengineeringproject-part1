import Vue from 'vue'
import Toasted from 'vue-toasted'

Vue.use(Toasted, {
    theme: "toasted-primary", 
    position: "top-center", 
    duration : 5000
});

// Lets Register a Global Error Notification Toast.
Vue.toasted.register('my_app_error', 
  (payload) => {
    if(! payload.message) {
      return "Something went wrong.."
    }
    return payload.message;
  }, {
    type : 'error'
  }
)

// Lets Register a Global Success Notification Toast.
Vue.toasted.register('my_app_success',
  (payload) => {
    if(! payload.message) {
      return "Success!"
    }
    return payload.message;
  }, {
    type : 'success'
  }
)